<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Thêm mới sản phẩm</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>
<?php //var_dump($get_show_tac_gia); exit;
    //var_dump($test);
?>
<form  action="?controller=Book&action=do_insert_book" method="POST" enctype="multipart/form-data">
    <h1 class="text-center">Thêm sách mới</h1>
    <div class="container bg-light">
        <?php if(isset($_SESSION['error_message'])): ?>
            <div class="alert alert-danger" role="alert">
            <?php echo ($_SESSION['error_message']); $_SESSION['error_message']=null;?>
        </div>
        <?php endif;?>

        <div class="container bg-light">
            <div class="mb-3">
                <label for="ten" class="form-label">Tên sách</label>
                <input type="text" name="ten" class="form-control" id="ten" placeholder="Tên sách" value="<?php echo isset($_SESSION['ten']) ? $_SESSION['ten'] : ""?>">
            </div>
            <div class="mb-3">
                <label for="loai_sach" class="form-label">Loại sách</label>
                <input type="text" name="loai_sach" class="form-control" id="loai_sach" placeholder="Loại sách" value="<?php echo isset($_SESSION['loai_sach']) ? $_SESSION['loai_sach'] : ""?>">
            </div>
            <div class="mb-3">
                <label for="mo_ta" class="form-label">Mô tả</label>
                <input type="text" name="mo_ta" class="form-control" id="mo_ta" placeholder="Mô tả" value="<?php echo isset($_SESSION['mo_ta']) ? $_SESSION['mo_ta'] : ""?>">
            </div>
            <div class="mb-3">
                <label for="tac_gia" class="form-label">Tác giả</label>
                <?php //echo isset($_SESSION['danh_muc']) ? $_SESSION['danh_muc'] : ""?>
                <select name="tac_gia" id="tac_gia" class="form-control">
                    <?php foreach($get_show_tac_gia as $tacGia): ?>
                        <option value="<?php echo $tacGia['id'];?>"><?php echo $tacGia['ten'];?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="nha_xuat_ban" class="form-label">Nhà xuất bản</label>
                <?php //echo isset($_SESSION['danh_muc']) ? $_SESSION['danh_muc'] : ""?>
                <select name="nha_xuat_ban" id="nha_xuat_ban" class="form-control">
                    <?php foreach($get_show_nha_xuat_ban as $nhaXuatBan): ?>
                        <option value="<?php echo $nhaXuatBan['id'];?>"><?php echo $nhaXuatBan['ten'];?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="so_luong" class="form-label">Số lượng</label>
                <input type="number" name="so_luong" class="form-control" id="so_luong" placeholder="Số lượng sản phẩm" value="<?php echo isset($_SESSION['so_luong']) ? $_SESSION['so_luong'] : ""?>">
            </div>
            <div class="mb-3">
                <label for="gia_ban" class="form-label">Giá bán sách</label>
                <input type="number" name="gia_ban" class="form-control" id="gia_ban" placeholder="Giá bán sản phẩm" value="<?php echo isset($_SESSION['gia_ban']) ? $_SESSION['gia_ban'] : ""?>">
            </div>

            <div class="mb-3">
                <label for="anh" class="form-label">Ảnh sản phẩm</label>
                <input accept="image/*" type="file" name="anh" class="form-control" id="anh" placeholder="Ảnh sản phẩm" value="<?php echo isset($_SESSION['anh']) ? $_SESSION['anh'] : ""?>">
            </div>
            <?php // echo ở post sẽ không lên nhưng nó vẫn có thông tin gửi theo form?>

            <label for="ngon_ngu"></label>
            <select name="ngon_ngu" id="ngon_ngu" class="form-control">
                    <option value="vi">Tiếng Việt</option>
                    <option value="en">Tiếng Anh</option>
                    <option value="fr">Tiếng Pháp</option>
                    <option value="cn">Tiếng Trung</option>
            </select>
            <div>
                <input type="reset" class="btn btn-outline-secondary">
                <input type="submit" class="btn btn-outline-primary" value="Thêm">
            </div>
        </div>
</form>
<?php //session_destroy();?>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</html>