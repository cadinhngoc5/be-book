<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Cập nhập thông tin sản phẩm</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>
<form  action="?controller=Book&action=do_update_book" method="POST" enctype="multipart/form-data">
    <h1 class="text-center">Sửa thông tin sách</h1>
    <div class="container bg-light">
        <?php if(isset($_SESSION['error_message'])): ?>
            <div class="alert alert-danger" role="alert">
            <?php echo ($_SESSION['error_message']); $_SESSION['error_message'] = null;?>
        </div>
        <?php endif;?>
        <?php if(isset($get_book_by_id)) {$book = $get_book_by_id;} ?>
        <div class="container bg-light">
            <input type="hidden" name="id" value="<?php echo isset($_SESSION['error_message'])?$_SESSION['id']:'';echo isset($book['id'])?$book['id']:'';?>">
            <div class="mb-3">
                <label for="ten" class="form-label">Tên sách</label>
                <input type="text" name="ten" class="form-control" id="ten" placeholder="Tên sách" value="<?php echo isset($_SESSION['error_message'])?$_SESSION['ten']:'';echo isset($book['ten'])?$book['ten']:'';?>">
            </div>
            <div class="mb-3">
                <label for="loai_sach" class="form-label">Loại sách</label>
                <input type="text" name="loai_sach" class="form-control" id="loai_sach" placeholder="Loại sách" value="<?php echo isset($_SESSION['error_message'])?$_SESSION['loai_sach']:'';echo isset($book['loai_sach'])?$book['loai_sach']:'';?>">
            </div>
            <div class="mb-3">
                <label for="mo_ta" class="form-label">Mô tả</label>
                <input type="text" name="mo_ta" class="form-control" id="mo_ta" placeholder="Mô tả" value="<?php echo isset($_SESSION['error_message'])?$_SESSION['mo_ta']:'';echo isset($book['mo_ta'])?$book['mo_ta']:'';?>">
            </div>

            <div class="mb-3">
                <label for="tac_gia" class="form-label">Tác giả</label>
                <?php //echo isset($_SESSION['danh_muc']) ? $_SESSION['danh_muc'] : ""?>
                <select name="tac_gia" id="tac_gia" class="form-control">
                    <?php foreach($get_show_tac_gia as $tacGia): ?>
                        <option value="<?php echo $tacGia['id'];?>" <?php echo isset($_SESSION['error_message']) && $_SESSION['id_tac_gia']== $tacGia['id'] ? 'selected' : ""; echo isset($book['id_tac_gia']) && $book['id_tac_gia']== $tacGia['id'] ? 'selected' : ''?>><?php echo $tacGia['ten'];?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="nha_xuat_ban" class="form-label">Nhà xuất bản</label>
                <?php //echo isset($_SESSION['danh_muc']) ? $_SESSION['danh_muc'] : ""?>
                <select name="nha_xuat_ban" id="nha_xuat_ban" class="form-control">
                    <?php foreach($get_show_nha_xuat_ban as $nhaXuatBan): ?>
                        <option value="<?php echo $nhaXuatBan['id'];?>" <?php echo isset($_SESSION['error_message']) && $_SESSION['id_nha_xuat_ban']== $nhaXuatBan['id'] ? 'selected' : ""; echo isset($book['id_nha_xuat_ban']) && $book['id_nha_xuat_ban']== $nhaXuatBan['id'] ? 'selected' : ''?>><?php echo $nhaXuatBan['ten'];?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="mb-3">
                <label for="so_luong" class="form-label">Số lượng</label>
                <input type="number" name="so_luong" class="form-control" id="so_luong" placeholder="Số lượng sách" value="<?php echo isset($_SESSION['error_message'])?$_SESSION['so_luong']:'';echo isset($book['so_luong'])?$book['so_luong']:'';?>">
            </div>
            <div class="mb-3">
                <label for="gia_ban" class="form-label">Giá bán sách</label>
                <input type="number" name="gia_ban" class="form-control" id="gia_ban" placeholder="Giá bán sản phẩm" value="<?php echo isset($_SESSION['error_message'])?$_SESSION['gia_ban']:'';echo isset($book['gia_ban'])?$book['gia_ban']:'';?>">
            </div>
            <div class="mb-3">
                <label for="anh_old" class="form-label">Ảnh sách hiện tại</label>
                <input type="text" name="anh_old" class="form-control" id="anh_old" placeholder="Ảnh sản phẩm hiện tại" value="<?php echo isset($_SESSION['error_message'])?$_SESSION['anh']:'';echo isset($book['anh'])?$book['anh']:'';?>">
            </div>
            <div class="mb-3">
                <label for="anh_new" class="form-label">Ảnh sách mới</label>
                <input accept="image/*" type="file" name="anh_new" class="form-control" id="anh_new" placeholder="Ảnh sản phẩm">
            </div>

            <label for="ngon_ngu"></label>
            <select name="ngon_ngu" id="ngon_ngu" class="form-control">
                <option value="vi" <?php echo isset($_SESSION['error_message']) && $_SESSION['ngon_ngu']== "vi" ? 'selected' : ""; echo isset($book['ngon_ngu']) && $book['ngon_ngu']=="vi" ? 'selected' : ''?>>Tiếng Việt</option>
                <option value="en" <?php echo isset($_SESSION['error_message']) && $_SESSION['ngon_ngu']== "en" ? 'selected' : ""; echo isset($book['ngon_ngu']) && $book['ngon_ngu']=="en" ? 'selected' : ''?>>Tiếng Anh</option>
                <option value="fr" <?php echo isset($_SESSION['error_message']) && $_SESSION['ngon_ngu']== "fr" ? 'selected' : ""; echo isset($book['ngon_ngu']) && $book['ngon_ngu']=="fr" ? 'selected' : ''?>>Tiếng Pháp</option>
                <option value="cn" <?php echo isset($_SESSION['error_message']) && $_SESSION['ngon_ngu']== "cn" ? 'selected' : ""; echo isset($book['ngon_ngu']) && $book['ngon_ngu']=="cn" ? 'selected' : ''?>>Tiếng Trung</option>
            </select>

            <div>
                <input type="reset" class="btn btn-outline-secondary">
                <input type="submit" class="btn btn-outline-primary" value="Sửa">
            </div>
        </div>
</form>
<?php //session_destroy();?>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</html>
