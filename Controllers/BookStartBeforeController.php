<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

class BookStartBeforeController extends CheckTokenController
{
    public function dateValid($date)
    {
        if (preg_match("/^(20[0-9]{2})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            return true;
        } else {
            return false;
        }
    }
    public function book_started_before()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $dataDecode = json_decode(file_get_contents("php://input"));
            //
            $result_check_existence_token = $this->check_existence_token();
            if (!isset($result_check_existence_token['token'])) {
                return $result_check_existence_token;
            }
            //
            $data = [];
            $message = "";
            if (!isset($dataDecode->id_book) || empty($dataDecode->id_book)) {
                $message .= 'Bạn thiếu mã sách cần mượn!';
            }

            if(!isset($dataDecode->intend_date) || empty($dataDecode->intend_date))
            {
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $data['intend_date'] = date('Y-m-d H:i:s');
            }
            else {
                $check_date = substr_count($dataDecode->intend_date, '-');
                $check_date1 = substr_count($dataDecode->intend_date, '+');
                //$check_date = $this->dateValid($dataDecode->intend_date);
                if($check_date == 2 and $check_date1 == 0)
                {
                    $date = explode("-", $dataDecode->intend_date);
                    $check_validate = checkdate($date[1], $date[2], $date[0]);
                    if (!$check_validate) {
                        $message .= 'Ngày không hợp lệ !';
                    }
                    if (strtotime($dataDecode->intend_date) < strtotime(date('Y-m-d H:i:s'))) {
                        $message .= 'Ngày dự kiến mượn đang nhỏ hơn ngày hiện tại!';
                    } else {
                        $data['intend_date'] = $dataDecode->intend_date;
                    }
                }
                else
                {
                    $message .= 'Ngày không hợp lệ !';
                }
            }

            $data['id_book'][0] = $dataDecode->id_book;
            $data['id_account'] = $result_check_existence_token['id_user'];
            $data['status_book'] = "Waiting";
            //$data['status_book_started_before'] = "Waiting";
            //$data['status_log_book'] = "borrowing";

            $borrowBook = $this->get_model("BorrowBookModel");
            $bookStartedBeforeModel = $this->get_model("BookStartedBeforeModel");

            $Book = $this->get_model("BookModel");
            $result = $Book->get_single_book($data['id_book'][0]);
            if ($result)
            {
                if ($result['status'] == "Not Available")
                {
                    $message = $message . ' Sách có mã: ' . $data['id_book'][0] . " đã được mượn!";
                    //$data['id_book_not_available'][] = $data['id_book'][0];
                }
                if ($result['status'] == "Waiting")
                {
                    $message = $message . ' Sách có mã: ' . $data['id_book'][0] . " đã được đặt trước!";
                }
                if ($result['status'] == "Expired")
                {
                    $message = $message . ' Sách có mã: ' . $data['id_book'][0] . " chưa được trả!";
                }
                if ($result['status'] == "Delete")
                {
                    $message = $message . ' Sách có mã: ' . $data['id_book'][0] . " đã bị xóa!";
                }
            }

            $count_book_borrowed = $borrowBook->count_book_borrowed($data['id_account']) + $bookStartedBeforeModel->count_book_started_before($data['id_account']);

            if ($count_book_borrowed >= 5) {
                $message .= 'Giới hạn cho phép mượn là 5 quyển';
            }
            //var_dump($data['id_book_not_available']);
            if (!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }
            $result = $bookStartedBeforeModel->book_started_before($data);
            $borrowBook->close_connect();
            if ($result)
            {
                //cập nhập lại status book
                $Book->update_status($data);
                $Book->close_connect();
                echo json_encode($this->form_json('201', $result, 'Create Successfully !'));
                return json_encode($this->form_json('201', $result, 'Create Successfully !'));
            } else
            {
                echo json_encode($this->form_json('400', '', 'Not be created !'));
                return json_encode($this->form_json('400', '', 'Not be created !'));

            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_all_book_started_before()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            $bookStartedBeforeModel = $this->get_model('BookStartedBeforeModel');
            $result = $bookStartedBeforeModel->get_all_list();
            $bookStartedBeforeModel->close_connect();
            $itemCount = count($result);
            if($itemCount > 0)
            {
                $userArr = array();
                $userArr["body"] = array();
                $userArr["body"] = $result;
                $userArr["itemCount"] = $itemCount;
                echo json_encode($this->form_json('200', $userArr, 'Successfuly !'));
                return json_encode($this->form_json('200', $userArr, 'Successfuly !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'No record found.'));
                return json_encode($this->form_json('404', '', 'No record found.'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_book_started_before_by_id_user()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }

            if(!isset($_GET['id_user']) || empty($_GET['id_user']))
            {
                $id_user = $result_check_existence_token['id_user'];
            }
            else
            {
                $id_user = $_GET['id_user'];
            }


//            if(!isset($id_user) || empty($id_user))
//            {
//                echo json_encode($this->form_json('400', '', 'Missing user code !'));
//                return json_encode($this->form_json('400', '', 'Missing user code !'));
//            }
            $bookStartedBeforeModel = $this->get_model('BookStartedBeforeModel');
            $result = $bookStartedBeforeModel->get_book_started_before_by_id_user($id_user);
            $bookStartedBeforeModel->close_connect();
            if($result)
            {

                $itemCount = count($result);
                if($itemCount > 0)
                {
                    $userArr = array();
                    $userArr["body"] = array();
                    $userArr["body"] = $result;
                    $userArr["itemCount"] = $itemCount;
                    echo json_encode($this->form_json('200', $userArr, 'Successfuly !'));
                    return json_encode($this->form_json('200', $userArr, 'Successfuly !'));
                }
                else
                {
                    echo json_encode($this->form_json('404', '', 'No record found.'));
                    return json_encode($this->form_json('404', '', 'No record found.'));
                }
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'No record found.'));
                return json_encode($this->form_json('404', '', 'No record found.'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }
}

