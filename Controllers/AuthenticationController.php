<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
class AuthenticationController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function register()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $admin_type = 1;
            $user_type = 2;
            $dataDecode = json_decode(file_get_contents("php://input"));
            $user = $this->get_model('UserModel');
            $tokenModel = $this->get_model('TokenModel');

            $message = "";
            if (!isset($dataDecode->email) || empty($dataDecode->email)) {
                $message .= 'Email không được bỏ trống !';
            }
            if (!isset($dataDecode->phone_number) || empty($dataDecode->phone_number)) {
                $message .= 'Số điện thoại không được bỏ trống !';
            }
            if( strlen($dataDecode->phone_number) < 10){
                $message .= 'Số điện thoại phải lớn hơn 10 kí tự !';
            }
            if (!isset($dataDecode->user_name) || empty($dataDecode->user_name)) {
                $message .= 'Username không được bỏ trống !';
            }
            if (!isset($dataDecode->user_password) || empty($dataDecode->user_password)) {
                $message .= 'User password không được bỏ trống !';
            }
            if( strlen($dataDecode->user_password) < 6){
                $message .= 'Mật khẩu phải lớn hơn 6 kí tự !';
            }

            if($user->check_field_existed("user_name",$dataDecode->user_name))
            {
                $message .= 'Username đã tồn tại !';
            }

            if($user->check_field_existed("email",$dataDecode->email))
            {
                $message .= 'Email đã tồn tại !';
            }

            if($user->check_field_existed("phone_number",$dataDecode->phone_number))
            {
                $message .= 'Phone number đã tồn tại !';
            }

            if($this->emailValid($dataDecode->email))
            {
                $message .= 'Email không đúng định dạng !';
            }

            if (!empty($message)) {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }

            $data = [];
            $data['email'] = $dataDecode->email;
            $data['phone_number'] = $dataDecode->phone_number;
            $data['user_name'] = $dataDecode->user_name;
            $data['user_password'] = md5($dataDecode->user_password);

            $result = $user->create($data);

            //start
            if($user->check_login($data))
            {
                $user = $user->get_user_by_username($data['user_name']);
                if($user['user_type'] == $admin_type)
                {
                    setcookie ("user_type",$admin_type,time()+ 30*24*60*60,"/");
                }
                else
                {
                    setcookie ("user_type",$user_type,time()+ 30*24*60*60,"/");
                }

                $token_create = $tokenModel->create_token($user['id']);
                if($token_create)
                {
                    $token_create['user_type'] = $user['user_type'];
                    $this->save_data_user_cookie($user['id'],$token_create['token']);
                    echo json_encode($this->form_json('201', $token_create, 'User created successfully !'));
                    return json_encode($this->form_json('201', $token_create, 'User created successfully !'));
                }
                else
                {
                    echo json_encode($this->form_json('400', '', 'User could not be created !'));
                    return json_encode($this->form_json('400', '', 'User could not be created !'));
                }
            }
            else
            {
                echo json_encode($this->form_json('400', '', 'User could not be created !'));
                return json_encode($this->form_json('400', '', 'User could not be created !'));
            }
        }
        else
        {
                echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
                return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $admin_type = 1;
            $user_type = 2;
            $dataDecode = json_decode(file_get_contents("php://input"));

            $message = "";
            if(!isset($dataDecode->user_name) || empty($dataDecode->user_name))
            {
                $message .= 'Username không được bỏ trống !';
            }

            if(!isset($dataDecode->user_password) || empty($dataDecode->user_password))
            {
                $message .= 'User password không được bỏ trống !';
            }

            if(!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }

            $data['user_name'] = $dataDecode->user_name;
            $data['user_password'] = md5($dataDecode->user_password);

            $users = $this->get_model('UserModel');
            $tokenModel = $this->get_model('TokenModel');

            if($users->check_login($data))
            {
                $user = $users->get_user_by_username($data['user_name']);

                if($user['status'] == 'Not valid')
                {
                    echo json_encode($this->form_json('403','', 'Account has been locked !'));
                    return json_encode($this->form_json('403','', 'Account has been locked !'));
                }
                if($user['user_type'] == $admin_type)
                {
                    setcookie ("user_type",$admin_type,time()+ 30*24*60*60,"/");
                }
                else
                {
                    setcookie ("user_type",$user_type,time()+ 30*24*60*60,"/");
                }

                if($token_alive = $tokenModel->check_token($user['id']))
                {
                    $token_alive['user_type'] = $users->get_user_type($user['id'])['user_type'];
                    $users->close_connect();
                    $tokenModel->close_connect();
                    $this->save_data_user_cookie($user['id'],$token_alive['token']);
                    echo json_encode($this->form_json('200', $token_alive, 'Logged in successfully !'));
                    return json_encode($this->form_json('200', $token_alive, 'Logged in successfully !'));
                }
                else
                {
                    $token_create = $tokenModel->create_token($user['id']);
                    if($token_create)
                    {
                        $token_create['user_type'] = $users->get_user_type($user['id'])['user_type'];
                        $users->close_connect();
                        $tokenModel->close_connect();
                        $this->save_data_user_cookie($user['id'],$token_create['token']);
                        echo json_encode($this->form_json('200', $token_create, 'Logged in successfully(With new token) !'));
                        return json_encode($this->form_json('200', $token_create, 'Logged in successfully(With new token) !'));
                    }
                    else
                    {
                        echo json_encode($this->form_json('404', '', 'Login failed(token not created) !'));
                        return json_encode($this->form_json('404', '', 'Login failed(token not created) !'));
                    }
                }
            }
            else
            {
                echo json_encode($this->form_json('400', '', 'Login failed !'));
                return json_encode($this->form_json('400', '', 'Login failed !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }
}
