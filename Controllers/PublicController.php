<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
class PublicController extends BaseController
{
    public function forget_password()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $userModel = $this->get_model('UserModel');
            $tokenModel = $this->get_model('tokenModel');
            $dataDecode = json_decode(file_get_contents("php://input"));
            $message = "";
            if (!isset($dataDecode->email) || empty($dataDecode->email)) {
                $message .= 'Email không được bỏ trống !';
            }
            if (!isset($dataDecode->phone_number) || empty($dataDecode->phone_number)) {
                $message .= 'Số điện thoại không được bỏ trống !';
            }
            if (!isset($dataDecode->user_name) || empty($dataDecode->user_name)) {
                $message .= 'Username không được bỏ trống !';
            }

            if (!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }

            $data = [];
            $data['email'] = $dataDecode->email;
            $data['phone_number'] = $dataDecode->phone_number;
            $data['user_name'] = $dataDecode->user_name;
            $result = $userModel->get_user_by_username($data['user_name']);
            $data['id_user'] = $result['id'];
            $result_token = $tokenModel->check_token($data['id_user']);

            if ($data['email'] === $result['email'] and $data['phone_number'] === $result['phone_number'])
            {
                echo json_encode($this->form_json('200',$result_token , "Xác nhận thông tin tài khoản thành công !"));
                return json_encode($this->form_json('200', $result_token, "Xác nhận thông tin tài khoản thành công !"));
            }
            else
            {
                echo json_encode($this->form_json('400', '', 'Thông tin không chính xác !'));
                return json_encode($this->form_json('400', '', 'Thông tin không chính xác !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_all_book()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $bookModel = $this->get_model('BookModel');
            $borrowBookModel = $this->get_model('BorrowBookModel');
            //update trạng thái sách quá hạn mượn mà chưa trả
            $expired = $borrowBookModel->get_expired_all_borrowed_book();
            $id_book_expired=[];
            for($i = 0; $i < count($expired); $i++)
            {
                $id_book_expired[] = $expired[$i]['id_book'];
            }
            $data=[];
            $data['id_book']=$id_book_expired;
            $data['status_book'] = 'Expired';
            $bookModel->update_status($data);
            //
            $result = $bookModel->get_all_book();
            echo json_encode($this->form_json('200', $result, 'Successfully !'));
            return json_encode($this->form_json('200', $result, 'Successfully !'));
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }

    }

    public function get_book_by_id()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {

            if(!isset($_GET['id_book']) || empty($_GET['id_book']))
            {
                echo json_encode($this->form_json('400', '', 'Missing book code !'));
                return json_encode($this->form_json('400', '', 'Missing book code !'));
            }
            else
            {
                $id_book = $_GET['id_book'];
            }
            $item = $this->get_model('BookModel');
            $result = $item->get_single_book($id_book);
            $item->close_connect();
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Not found!'));
                return json_encode($this->form_json('404', '', 'Not found!'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }
}
