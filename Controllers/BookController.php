<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

class BookController extends CheckTokenController
{
//    public function __construct()
//    {
//        //$headers = apache_request_headers();
//        //$token = $headers['token'];
//        parent::__construct();
//    }
    public function create_book()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $dataDecode = json_decode(file_get_contents("php://input"));
            //
            $result_check_existence_token = $this->check_existence_token();
            if (!isset($result_check_existence_token['token'])) {
                return $result_check_existence_token;
            }
//            if ($_SESSION['user_type'] == $_SESSION['users_type']) {
//                echo json_encode($this->form_json('403', "", 'You do not have access !'));
//                return json_encode($this->form_json('403', "", 'You do not have access !'));
//            }

            $message = "";
            if (!isset($dataDecode->book_name) || empty($dataDecode->book_name))
            {
                $message .= 'Tên không được bỏ trống !';
            }

            if (!isset($dataDecode->book_type) || empty($dataDecode->book_type))
            {
                $message .= 'Loại sách không được bỏ trống !';
            }

            if (!isset($dataDecode->book_describe) || empty($dataDecode->book_describe))
            {
                $message .= 'Mô tả sách không được bỏ trống !';
            }

            if (!isset($dataDecode->book_author) || empty($dataDecode->book_author))
            {
                $message .= 'Tên tác giả không được bỏ trống !';
            }

            if (!isset($dataDecode->book_publisher) || empty($dataDecode->book_publisher))
            {
                $message .= 'Tên nhà xuất bản không được bỏ trống !';
            }

            if ($dataDecode->book_cost < 0)
            {
                $message .= 'Giá bán sách phải lớn hơn hoặc bằng 0 !';
            }

            if (!isset($dataDecode->book_image) || empty($dataDecode->book_image))
            {
                $message .= 'Link ảnh sách không được bỏ trống !';
            }

            if (!isset($dataDecode->book_language) || empty($dataDecode->book_language))
            {
                $message .= 'Ngôn ngữ sách không được bỏ trống !';
            }

            if (!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }

            $item = $this->get_model('BookModel');

            $data = [];
            $data['book_name'] = $dataDecode->book_name;
            $data['book_type'] = $dataDecode->book_type;
            $data['book_describe'] = $dataDecode->book_describe;
            $data['book_author'] = $dataDecode->book_author;
            $data['book_publisher'] = $dataDecode->book_publisher;
            $data['book_cost'] = $dataDecode->book_cost;
            $data['book_image'] = $dataDecode->book_image;
            $data['book_language'] = $dataDecode->book_language;

            $result = $item->create_book($data);
            $item->close_connect();
            if ($result) {
                echo json_encode($this->form_json('201', "", 'Create Successfully !'));
                return json_encode($this->form_json('201', "", 'Create Successfully !'));
            } else {
                echo json_encode($this->form_json('400', '', 'Not be created !'));
                return json_encode($this->form_json('400', '', 'Not be created !'));
            }
        }
        else
        {
                echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
                return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function update_book()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            $dataDecode = json_decode(file_get_contents("php://input"));
            //
            $result_check_existence_token = $this->check_existence_token();
            if (!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
//            if (!$this->check_user_type($dataDecode->user_type)) {
//                echo json_encode($this->form_json('403', "", 'You do not have access !'));
//                return json_encode($this->form_json('403', "", 'You do not have access !'));
//            }

            //validate
            $message = "";
            if(!isset($dataDecode->id) || empty($dataDecode->id))
            {
                $message .= 'Mã sách không được bỏ trống !<br>';
            }

            if (!isset($dataDecode->book_name) || empty($dataDecode->book_name))
            {
                $message .= 'Tên không được bỏ trống !<br>';
            }

            if (!isset($dataDecode->book_type) || empty($dataDecode->book_type))
            {
                $message .= 'Loại sách không được bỏ trống !<br>';
            }

            if (!isset($dataDecode->book_describe) || empty($dataDecode->book_describe))
            {
                $message .= 'Mô tả sách không được bỏ trống !<br>';
            }

            if(!isset($dataDecode->book_author) || empty($dataDecode->book_author))
            {
                $message .= 'Tên tác giả không được bỏ trống !<br>';
            }

            if(!isset($dataDecode->book_publisher) || empty($dataDecode->book_publisher))
            {
                $message .= 'Tên nhà xuất bản không được bỏ trống !<br>';
            }

            if ($dataDecode->book_cost < 0)
            {
                $message .= 'Giá bán sách phải lớn hơn hoặc bằng 0 !<br>';
            }

            if (!isset($dataDecode->book_image) || empty($dataDecode->book_image))
            {
                $message .= 'Link ảnh sách không được bỏ trống !<br>';
            }

            if (!isset($dataDecode->book_language) || empty($dataDecode->book_language))
            {
                $message .= 'Ngôn ngữ sách không được bỏ trống !<br>';
            }

            if(!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }

            $data = [];
            $data['id'] = $dataDecode->id;
            $data['book_name'] = $dataDecode->book_name;
            $data['book_type'] = $dataDecode->book_type;
            $data['book_describe'] = $dataDecode->book_describe;
            $data['book_author'] = $dataDecode->book_author;
            $data['book_publisher'] = $dataDecode->book_publisher;
            $data['book_cost'] = $dataDecode->book_cost;
            $data['book_image'] = $dataDecode->book_image;
            $data['book_language'] = $dataDecode->book_language;

            $book = $this->get_model('BookModel');
            $result = $book->update_book($data);
            $book->close_connect();
            if ($result)
            {
                echo json_encode($this->form_json('200', '', 'Book update successfuly !'));
                return json_encode($this->form_json('200', '', 'Book update successfuly !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Missing book code !'));
                return json_encode($this->form_json('404', '', 'Missing book code !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function delete_book()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'DELETE')
        {
            //$dataDecode = json_decode(file_get_contents("php://input"));
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            $message="";
            if(!isset($_GET['id_book']) || empty($_GET['id_book']))
            {
                $message .= 'Bạn thiếu mã sách!';
            }
            else
            {
                $id_book = $_GET['id_book'];
            }
            $data=[];
            $book = $this->get_model("BookModel");
            $data['status_book'] = "Delete";
            $data['id_book'][0] = $id_book;
            $infomation_book = $book->get_single_book($data['id_book'][0]);
            if($infomation_book)
            {
                if ($infomation_book['status'] == "Not Available")
                {
                    $message = $message . ' Sách có mã: ' . $data['id_book'][0] . " đã được mượn! Bạn không được xóa !";
                }
                if ($infomation_book['status'] == "Waiting")
                {
                    $message = $message . ' Sách có mã: ' . $data['id_book'][0] . " đã được đặt trước! Bạn không được xóa !";
                }
                if (($infomation_book['status'] == "Delete"))
                {
                    $message = $message . ' Sách có mã: ' . $data['id_book'][0] . " đã được xóa! Bạn không được xóa !";
                }
                if (($infomation_book['status'] == "Expired"))
                {
                    $message = $message . ' Sách có mã: ' . $data['id_book'][0] . " chưa được trả! Bạn không được xóa !";
                }
            }
            else
            {
                $message = $message . "Không có sách trong cơ sở dữ liệu";
            }

            if (!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }
            $result = $book->update_status($data);
            if($result)
            {
                echo json_encode($this->form_json('200', '', 'Xóa sách thành công !'));
                return json_encode($this->form_json('200', '', 'Xóa sách thành công !'));
            }
            else
            {
                echo json_encode($this->form_json('400', '', 'Xóa sách không thành công !'));
                return json_encode($this->form_json('200', '', 'Xóa sách không thành công !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }
}
?>

