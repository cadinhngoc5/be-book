<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

class BookExpiredController extends CheckTokenController
{
    public function get_all_book_expired()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            $borrowBookModel = $this->get_model('BorrowBookModel');
            $bookModel = $this->get_model('BookModel');
            //lấy tất cả các sách đã quá hạn trong database
            $expired = $borrowBookModel->get_expired_all_borrowed_book();
            $result=[];
            for($i = 0; $i < count($expired); $i++)
            {
                $result[] = $bookModel->get_single_book($expired[$i]['id_book']);
            }
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Not found'));
                return json_encode($this->form_json('404', '', 'Not found'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }
    public function get_expired_borrowed_book_by_id_user()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            if(!isset($_GET['id_user']) || empty($_GET['id_user']))
            {
                $id_user = $result_check_existence_token['id_user'];
            }
            else
            {
                $id_user = $_GET['id_user'];
            }

            $borrowBookModel = $this->get_model('BorrowBookModel');
            $bookModel = $this->get_model('BookModel');
            //lấy tất cả các sách đã quá hạn của người dùng
            $expired = $borrowBookModel->get_expired_borrowed_book_by_id_user($id_user);
            $result=[];
            for($i = 0; $i < count($expired); $i++)
            {
                $result[] = $bookModel->get_single_book($expired[$i]['id_book']);
            }
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Not found'));
                return json_encode($this->form_json('404', '', 'Not found'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }
}
?>


