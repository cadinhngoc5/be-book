<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers , Authorization, X-Requested-With");
class BaseController
{
    public function __construct()
    {
        session_start();
//        $_SESSION['admin_type'] = 1;
//        $_SESSION['users_type'] = 2;
//        setcookie ("user_type",$_SESSION['admin_type'],time()+ 30*24*60*60,"/");
    }

    public function render_view($viewName, $data=[])
    {
        extract($data);
        include('Views/'.$viewName.'.php');
    }

    public function get_model($modelName)
    {
        include('Models/'.$modelName.'.php');
        $modelObj = new $modelName();
        return $modelObj;
    }

    public function render_layout($layoutName, $views=[], $data=[])
    {
        $params = [
            'views' => $views,
            'data' => $data,
        ];
        extract($params);
        include('Views/Layout/'.$layoutName.'.php');
    }

    public function redirect($controller, $action)
    {
        header("Location: ?controller=".$controller."&action=".$action);
    }

    public function form_json($status, $data, $message)
    {
        $status = (int)$status;
        $result=['status' => $status, 'data'=>$data, 'message'=>$message];
//        http_response_code($status);
        if($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'POST' ||
        $_SERVER['REQUEST_METHOD'] == 'PUT' || $_SERVER['REQUEST_METHOD'] == 'DELETE' )
        {
            http_response_code($status);
        }
        return $result;

    }

    public function save_data_user_cookie($id, $token)
    {
        setcookie ("id",$id,time()+ 30*24*60*60,"/");
        setcookie ("token",$token,time()+ 30*24*60*60,"/");
    }

    public function emailValid($email)
    {
        $regex = "/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i";
        if(!preg_match($regex, $email)) {
            return true;
        }
        else
        {
            return false;
        }
    }
}