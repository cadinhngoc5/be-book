<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
class CheckTokenController extends BaseController
{
    public function check_existence_token()
    {
        $token="";
        $type="";
        $requestHeaders = apache_request_headers();
        $Authorization = "";
        $Authorization = trim($requestHeaders['Authorization']);

        if (isset($Authorization))
        {
            //list($type, $data) = explode(" ", $Authorization);
            $typeAndToken = explode(" ", $Authorization);
            if(isset($typeAndToken[0]))
            {
                $type= $typeAndToken[0];
            }
            if(isset($typeAndToken[1]))
            {
                $token= $typeAndToken[1];
            }

            if (!strcasecmp($type, "Bearer") == 0)
            {
                echo(json_encode($this->form_json('403', "", 'Kiểu đăng nhập mặc đinh: Bearer !')));
                return(json_encode($this->form_json('403', "", 'Kiểu đăng nhập mặc đinh: Bearer !')));
            }
        }
        else
        {
            echo(json_encode($this->form_json('400', "", 'Không xác thực - '.$Authorization)));
            return(json_encode($this->form_json('400', "", 'Không xác thực - '.$Authorization)));
        }
        //check token
        $tokenModel = $this->get_model('TokenModel');
        $result_check_token_from_header = $tokenModel->check_token_from_header($token);
        if(!$result_check_token_from_header)
        {
            echo(json_encode($this->form_json('404', "", 'Không tìm thấy token trong database !')));
            return(json_encode($this->form_json('404', "", 'Không tìm thấy token trong database !')));
        }

        //để xem tí nữa cần không
        //setcookie ("token",$token,time()+ 30*24*60*60,"/");

        if(strtotime($result_check_token_from_header['refresh_token_expried']) < strtotime(date('Y-m-d H:i:s')))
        {
            $result = $tokenModel->delete_token_with_token($token);
            if($result)
            {
                setcookie ("user_type","",time()- 30,"/");
                setcookie ("token","",time()- 30,"/");
                setcookie ("id","",time()- 30,"/");
                //$tokenModel->close_connect();
            }
            //$tokenModel->close_connect();
            echo(json_encode($this->form_json('401', "", 'Refresh Token đã hết hạn ! Tài khoản tự động đăng xuất ! Hãy đăng nhập lại !')));
            return(json_encode($this->form_json('401', "", 'Refresh Token đã hết hạn ! Tài khoản tự động đăng xuất ! Hãy đăng nhập lại !')));
        }

        if(strtotime($result_check_token_from_header['token_expried']) < strtotime(date('Y-m-d H:i:s')))
        {
            echo json_encode("Token đã hết hạn !");
            echo json_encode("Token sẽ được tự động gia hạn!");
            $newToken = $tokenModel->update_token($token);
            //$tokenModel->close_connect();
            //$_SESSION['token']=$newToken['token'];
            setcookie ("token",$newToken['token'],time()+ 30*24*60*60,"/");
            echo json_encode($this->form_json('201', $newToken, 'Tạo token thành cônng !'));
            return json_encode($this->form_json('201', $newToken, 'Tạo token thành cônng !'));
        }
        //$tokenModel->close_connect();
        return $result_check_token_from_header;
    }

    public function check_user_type($type)
    {
        $admin = 1;
        $user = 2;
        if($type == $admin)
        {
            return true;
        }
        return false;
    }
}
