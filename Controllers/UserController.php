<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

class UserController extends CheckTokenController
{
    public function update_user()//20-12-2022
    {
        if ($_SERVER['REQUEST_METHOD'] === 'PUT')
        {
            $dataDecode = json_decode(file_get_contents("php://input"));
            $data= [];
            $user = $this->get_model('UserModel');
            //
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            $infomation_user = $user->get_single_user($result_check_existence_token['id_user']);
            //
            $message = "";
            if (!isset($dataDecode->lastname) || empty($dataDecode->lastname))
            {
                $message .= 'Họ không được bỏ trống !';
            }

            if (!isset($dataDecode->firstname) || empty($dataDecode->firstname))
            {
                $message .= 'Tên không được bỏ trống !';
            }

            if (!isset($dataDecode->email) || empty($dataDecode->email))
            {
                $message .= 'Email không được bỏ trống !';
            }

            if (!isset($dataDecode->phone_number) || empty($dataDecode->phone_number))
            {
                $message .= 'Số điện thoại không được bổ trống !';
            }

            if( strlen($dataDecode->phone_number) < 10){
                $message .= 'Số điện thoại phải lớn hơn 10 kí tự !';
            }

            if (!isset($dataDecode->address) || empty($dataDecode->address))
            {
                $message .= 'Địa chỉ không được bỏ trống !';
            }

            if (!isset($dataDecode->sex) || empty($dataDecode->sex))
            {
                $message .= 'Giới tính không được bỏ trống !';
            }

            if($infomation_user['email'] == $dataDecode->email){
                $data['email'] = $dataDecode->email;
            }
            else
            {
                if($user->check_field_existed("email",$dataDecode->email))
                {
                    $message .= 'Email đã tồn tại !';
                }
                else
                {
                    $data['email'] = $dataDecode->email;
                }
            }

            if($infomation_user['phone_number'] == $dataDecode->phone_number){
                $data['phone_number'] = $dataDecode->phone_number;
            }
            else
            {
                if($user->check_field_existed("phone_number", $dataDecode->phone_number))
                {
                    $message .= 'Phone number đã tồn tại !';
                }
                else
                {
                    $data['phone_number'] = $dataDecode->phone_number;
                }
            }

            if($this->emailValid($dataDecode->email))
            {
                $message .= 'Email không đúng định dạng !';
            }
            //2022-12-29
            if (!isset($dataDecode->year) || empty($dataDecode->year))
            {
                $message .= 'Năm sinh không được bổ trống !';
            }
            if( $dataDecode->year < 1900 ){
                $message .= 'Năm sinh phải lớn hơn 1900!';
            }

            if (!isset($dataDecode->month) || empty($dataDecode->month))
            {
                $message .= 'Tháng không được bổ trống !';
            }
            if( $dataDecode->month < 1 || $dataDecode->month > 12){
                $message .= 'Tháng phải lớn hơn 0 và nhỏ hơn 13!';
            }

            if (!isset($dataDecode->day) || empty($dataDecode->day))
            {
                $message .= 'Ngày không được bổ trống !';
            }
            if( $dataDecode->day < 1 || $dataDecode->day > 31){
                $message .= 'Ngày phải lớn hơn 0 và nhỏ hơn 32!';
            }

            if(!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }

            $data['id'] = $result_check_existence_token['id_user'];
            $data['lastname'] = $dataDecode->lastname;
            $data['firstname'] = $dataDecode->firstname;
            //$data['email'] = $dataDecode->email;
            $data['phone_number'] = $dataDecode->phone_number;
            $data['address'] = $dataDecode->address;
            $data['sex'] = $dataDecode->sex;
            $data['date_of_birth'] = $dataDecode->year."-".$dataDecode->month."-".$dataDecode->day;


            $result = $user->update_user($data);
            $user->close_connect();
            if ($result)
            {
                echo json_encode($this->form_json('200', '', 'User update successfuly !'));
                return json_encode($this->form_json('200', '', 'User update successfuly !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'User update failed !'));
                return json_encode($this->form_json('404', '', 'User update failed !'));
            }

        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_user_by_id()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            if(!isset($_GET['id_user']) || empty($_GET['id_user']))
            {
                $id_user = $result_check_existence_token['id_user'];
            }
            else
            {
                $id_user = $_GET['id_user'];
            }
            $user = $this->get_model('UserModel');
            $result = $user->get_single_user($id_user);
            $user->close_connect();
            $result['year'] = substr($result['date_of_birth'], 0, 4);
            $result['month'] = substr($result['date_of_birth'], 5, 2);
            $result['day'] = substr($result['date_of_birth'], 8, 2);
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfuly !'));
                return json_encode($this->form_json('200', $result, 'Successfuly !'));
            }
            else
            {
                echo json_encode($this->form_json('404', "", 'User does not exist !'));
                return json_encode($this->form_json('404', "", 'User does not exist !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_all_user()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            $user = $this->get_model('UserModel');
            $result = $user->get_all_user();
            $user->close_connect();
            $itemCount = count($result);
            if($itemCount > 0)
            {
                $userArr = array();
                $userArr["body"] = array();
                $userArr["body"] = $result;
                $userArr["itemCount"] = $itemCount;
                echo json_encode($this->form_json('200', $userArr, 'Successfuly !'));
                return json_encode($this->form_json('200', $userArr, 'Successfuly !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'No record found.'));
                return json_encode($this->form_json('404', '', 'No record found.'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function change_password()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            $dataDecode = json_decode(file_get_contents("php://input"));
            //
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            $data= [];
            $message = "";
            $user = $this->get_model('UserModel');
            $data['id_user'] = $result_check_existence_token['id_user'];
            if (!isset($dataDecode->old_password) || empty($dataDecode->old_password))
            {
                $message .= 'Mật khẩu hiện tại không được bỏ trống !';
            }
            else
            {
                $data['old_password'] = md5($dataDecode->old_password);
                $result_check = $user->check_password($data);
                if(!$result_check)
                {
                    $message .= 'Mật khẩu cũ không chính xác !';
                }
            }
            //thêm vào check mật khẩu mới và xác nhận mật khẩu mới
            if (!isset($dataDecode->new_password) || empty($dataDecode->new_password))
            {
                $message .= 'Mật khẩu mới không được bỏ trống !';
            }
            if( strlen($dataDecode->new_password) < 6){
                $message .= 'Mật khẩu mới phải lớn hơn 6 kí tự !';
            }
            if (!isset($dataDecode->confirm_new_password) || empty($dataDecode->confirm_new_password))
            {
                $message .= 'Xác nhận mật khẩu mới không được bỏ trống !';
            }

            if($dataDecode->new_password === $dataDecode->confirm_new_password)
            {
                $data['new_password'] = md5($dataDecode->new_password);
            }
            else
            {
                $message .= 'Xác nhận mật khẩu mới không chính xác !';
            }

            if(!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }

            $result = $user->change_password($data);
            $user->close_connect();
            if ($result)
            {
                echo json_encode($this->form_json('200', '', 'Password update successful !'));
                return json_encode($this->form_json('200', '', 'Password update successful !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Password update failed !'));
                return json_encode($this->form_json('404', '', 'Password update failed !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function change_password_for_forget_password()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            $dataDecode = json_decode(file_get_contents("php://input"));
            //
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            $data= [];
            $message = "";
            $user = $this->get_model('UserModel');
            $data['id_user'] = $result_check_existence_token['id_user'];
            //thêm vào check mật khẩu mới và xác nhận mật khẩu mới
            if (!isset($dataDecode->new_password) || empty($dataDecode->new_password))
            {
                $message .= 'Mật khẩu mới không được bỏ trống !';
            }
            if( strlen($dataDecode->new_password) < 6){
                $message .= 'Mật khẩu mới phải lớn hơn 6 kí tự !';
            }
            if (!isset($dataDecode->confirm_new_password) || empty($dataDecode->confirm_new_password))
            {
                $message .= 'Xác nhận mật khẩu mới không được bỏ trống !';
            }

            if($dataDecode->new_password === $dataDecode->confirm_new_password)
            {
                $data['new_password'] = md5($dataDecode->new_password);
            }
            else
            {
                $message .= 'Xác nhận mật khẩu mới không chính xác !';
            }

            if(!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }

            $result = $user->change_password($data);
            if ($result)
            {
                $result_check_existence_token['user_type'] = $user->get_user_type($data['id_user'])['user_type'];
                $user->close_connect();
                echo json_encode($this->form_json('200', $result_check_existence_token, 'Password update successful !'));
                return json_encode($this->form_json('200', $result_check_existence_token, 'Password update successful !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Password update failed !'));
                return json_encode($this->form_json('404', '', 'Password update failed !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function delete_user_by_id()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'DELETE')
        {
            //$dataDecode = json_decode(file_get_contents("php://input"));
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }

            $message="";
            if(!isset($_GET['id_user']) || empty($_GET['id_user']))
            {
                $message .= 'Bạn thiếu mã người dùng!';
            }
            $id = $_GET['id_user'];
            $data=[];
            $user = $this->get_model("UserModel");
            $data['status_user'] = "Not valid";
            $data['id_user'] = $id;
            $infomation_user = $user->get_single_user($data['id_user']);
            if(!$infomation_user)
            {
                echo json_encode($this->form_json('404', "", "Not found user !"));
                return json_encode($this->form_json('404', "", "Not found user !"));
            }
            if (($infomation_user['status'] == "Not valid"))
            {
                $message = $message . ' User có mã: ' . $data['id_user'] . " đã được xóa!";
            }

            if (!empty($message))
            {
                echo json_encode($this->form_json('400', "", $message));
                return json_encode($this->form_json('400', "", $message));
            }
            $result = $user->update_status($data);
            //delete token
            $user->delete_token_with_id_user($data['id_user']);
            if($result)
            {
                echo json_encode($this->form_json('200', '', 'Xóa user thành công !'));
                return json_encode($this->form_json('200', '', 'Xóa user thành công !'));
            }
            else
            {
                echo json_encode($this->form_json('400', '', 'Xóa user không thành công !'));
                return json_encode($this->form_json('200', '', 'Xóa user không thành công !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }
}