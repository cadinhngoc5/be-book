<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

class BorrowBookController extends CheckTokenController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function borrow_book()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $dataDecode = json_decode(file_get_contents("php://input"));
            //
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            $data=[];
            $data['status_book'] = "Not Available";
            $data['status_book_started_before'] = "Borrowing";
            //$data['status_log_book'] = "borrowing";

            $message="";
            if(!isset($dataDecode->id_promissory_note) || empty($dataDecode->id_promissory_note))
            {
                $message .= 'Bạn thiếu mã phiếu đặt trước!';
            }
            if(!isset($dataDecode->borrow_days) || empty($dataDecode->borrow_days))
            {
                $message .= 'Bạn thiếu số ngày mượn!';
            }

            $data['id_promissory_note'] = $dataDecode->id_promissory_note;
            $bookStartedBeforeModel = $this->get_model("BookStartedBeforeModel");
            $Book = $this->get_model("BookModel");
            $infomation_promissory_note = $bookStartedBeforeModel->get_book_started_before_by_id_promissory_note($data['id_promissory_note']);

            if($infomation_promissory_note)
            {
                $data['borrow_days'] = $dataDecode->borrow_days;
                $data['id_book'][0] = $infomation_promissory_note[0]['id_book'];
                $data['id_account'] = $infomation_promissory_note[0]['id_account'];
                $data['expiration_date'] = $infomation_promissory_note[0]['expiration_date'];
                if(strtotime($data['expiration_date']) < strtotime(date('Y-m-d H:i:s')))
                {
                    $data['status_book'] = "Available";
                    $data['status_book_started_before'] = "Expired";
                    $bookStartedBeforeModel->update_status($data);
                    $Book->update_status($data);
                    $message .= 'Phiếu đặt trước đã quá hạn';
                }
                //$data['borrow_start_date'] = $dataDecode->borrow_start_date;

                $borrowBook = $this->get_model("BorrowBookModel");

                if($data['borrow_days'] > 10)
                {
                    $message .= 'Giới hạn cho phép mượn là 10 ngày';
                }

                $result = $Book->get_single_book($data['id_book'][0]);
                if($result)
                {
                    if($result['status'] == "Not Available")
                    {
                        $message = $message. ' Sách có mã: '.$data['id_book'][0]. " đã được mượn!";
                        //$data['id_book_not_available'][]= $data['id_book'][$i];
                    }
                }

                $count_book_borrowed = $borrowBook->count_book_borrowed($data['id_account']);
                $count_book = $count_book_borrowed + Count($data['id_book']);
                if($count_book >5)
                {
                    $message .= 'Giới hạn cho phép mượn là 5 quyển';
                    $message .= 'Bạn đã mượn: '.$count_book_borrowed." quyển";
                }

                //var_dump($data['id_book_not_available']);
                if(!empty($message))
                {
                    echo json_encode($this->form_json('400', "", $message));
                    return json_encode($this->form_json('400', "", $message));
                }
            }
            else
            {
                echo json_encode($this->form_json('404', "", "Không có mã đặt trước !"));
                return json_encode($this->form_json('404', "", "Không có mã đặt trước !"));
            }


            $result = $borrowBook->borrow_book($data);
            $borrowBook->close_connect();
            if($result)
            {
                //cập nhập lại status book
                $Book->update_status($data);
                //cap nhap lai status book_started_before
                $bookStartedBeforeModel->update_status($data);
                $Book->close_connect();
                echo json_encode($this->form_json('201', $result, 'Create Successfully !'));
                return json_encode($this->form_json('201', $result, 'Create Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('400', '', 'Not be created !'));
                return json_encode($this->form_json('400', '', 'Not be created !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_all_borrow_book()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $borrowBook = $this->get_model("BorrowBookModel");
            $result = $borrowBook->get_all_borrow_book();
            $borrowBook->close_connect();
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'No record found !'));
                return json_encode($this->form_json('404', '', 'No record found !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_all_borrowing_book()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            $data=[];
            $data['status_log_book'] = "Borrowing";
            $borrowBook = $this->get_model("BorrowBookModel");
            $result = $borrowBook->get_all_borrowing_book($data);
            $borrowBook->close_connect();
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Not found!'));
                return json_encode($this->form_json('404', '', 'Not found!'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_all_borrowed_book()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            $data=[];
            $data['status_log_book'] = "Borrowing";
            $borrowBook = $this->get_model("BorrowBookModel");
            $result = $borrowBook->get_all_borrowed_book($data);
            $borrowBook->close_connect();
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Not found!'));
                return json_encode($this->form_json('404', '', 'Not found!'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_history_borrow_book_by_id_user()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            if(!isset($_GET['id_user']) || empty($_GET['id_user']))
            {
                $id_user = $result_check_existence_token['id_user'];
            }
            else
            {
                $id_user = $_GET['id_user'];
            }
            //
            $data=[];
            $result=[];
            $data['id_user'] = $id_user;
            $data['status_log_book'] = "borrowing";

            $bookStartedBeforeModel = $this->get_model('BookStartedBeforeModel');
            $bookModel = $this->get_model('BookModel');

            $result1 = $bookStartedBeforeModel->get_book_started_before_by_id_user($id_user);
            if($result1)
            {
                $result11=[];
                for($i = 0; $i < count($result1); $i++){
                    $result11[] = $bookModel->get_single_book($result1[$i]['id_book']);
                }
                for($i=0; $i < count($result11); $i++)
                {
                    $result11[$i]['id_code'] = $result1[$i]['id'];
                }
                $bookStartedBeforeModel->close_connect();
            }
            else
            {
                $result11 = NULL;
            }

            $data['status_log_book'] = "borrowing";
            $borrowBook = $this->get_model("BorrowBookModel");
            $result2 = $borrowBook->get_history_book_by_id_user($data);
            if($result2)
            {
                $result22=[];
                for($i = 0; $i < count($result2); $i++){
                    $result22[] = $bookModel->get_single_book($result2[$i]['id_book']);
                }
                for($i=0; $i < count($result22); $i++)
                {
                    $result22[$i]['id_code'] = $result2[$i]['id'];
                }
            }
            else
            {
                $result22 = NULL;
            }


            $data['status_log_book'] = "Returned";
            $result3 = $borrowBook->get_history_book_by_id_user($data);
            if($result3)
            {
                $result33 = [];
                for ($i = 0; $i < count($result3); $i++) {
                    $result33[] = $bookModel->get_single_book($result3[$i]['id_book']);
                }
                for($i=0; $i < count($result33); $i++)
                {
                    $result33[$i]['id_code'] = $result3[$i]['id'];
                }
            }
            else
            {
                $result33 = NULL;
            }

            $data['status_log_book'] = "Expired";
            $result4 = $borrowBook->get_expired_borrowed_book_by_id_user($data['id_user']);
            if($result4)
            {
                $result44 = [];
                for ($i = 0; $i < count($result4); $i++) {
                    $result44[] = $bookModel->get_single_book($result4[$i]['id_book']);
                }
                for($i=0; $i < count($result44); $i++)
                {
                    $result44[$i]['id_code'] = $result4[$i]['id'];
                }
                $borrowBook->close_connect();
            }
            else
            {
                $result44 = NULL;
            }

            $result['book_started_before'] = $result11;
            $result['borrowing_book'] = $result22;
            $result['borrowed_book'] = $result33;
            $result['expiring_book'] = $result44;


            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Not found!'));
                return json_encode($this->form_json('404', '', 'Not found!'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_borrowing_book_by_id_user()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            if(!isset($_GET['id_user']) || empty($_GET['id_user']))
            {
                $id_user = $result_check_existence_token['id_user'];
            }
            else
            {
                $id_user = $_GET['id_user'];
            }
            //
            $data=[];
            $data['id_user'] = $id_user;
            $data['status_log_book'] = "borrowing";
            $borrowBook = $this->get_model("BorrowBookModel");
            $result = $borrowBook->get_borrowing_book_by_id_user($data);
            $borrowBook->close_connect();
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Not found!'));
                return json_encode($this->form_json('404', '', 'Not found!'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function get_borrowed_book_by_id_user()//lấy ra sách đã mượn bằng id người dùng
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            if(!isset($_GET['id_user']) || empty($_GET['id_user']))
            {
                $id_user = $result_check_existence_token['id_user'];
            }
            else
            {
                $id_user = $_GET['id_user'];
            }
            $data=[];
            $data['id_user'] = $id_user;
            $data['status_log_book'] = "Borrowing";
            $borrowBook = $this->get_model("BorrowBookModel");
            $result = $borrowBook->get_borrowed_book_by_id_user($data);
            $borrowBook->close_connect();
            if($result)
            {
                echo json_encode($this->form_json('200', $result, 'Successfully !'));
                return json_encode($this->form_json('200', $result, 'Successfully !'));
            }
            else
            {
                echo json_encode($this->form_json('404', '', 'Not found!'));
                return json_encode($this->form_json('404', '', 'Not found!'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }

    public function give_book_back()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $dataDecode = json_decode(file_get_contents("php://input"));
            //
            $result_check_existence_token = $this->check_existence_token();
            if(!isset($result_check_existence_token['token']))
            {
                return $result_check_existence_token;
            }
            //
            $data=[];

            $message="";
            if(!isset($dataDecode->id_log_book) || empty($dataDecode->id_log_book))
            {
                $message .= 'Bạn thiếu mã phiếu mượn!';
            }

            $data['id_log_book'] = $dataDecode->id_log_book;
            $borrowBook = $this->get_model("BorrowBookModel");
            $infomation_borrowBook = $borrowBook->get_borrow_book_by_id_log_book($data['id_log_book']);

            if($infomation_borrowBook)
            {
                if($infomation_borrowBook[0]['status'] == "Expired" || $infomation_borrowBook[0]['status'] == "Returned")
                {
                    echo json_encode($this->form_json('404', "", 'Sách đã được trả từ trước !'));
                    return json_encode($this->form_json('404', "", 'Sách đã được trả từ trước !'));
                }
                $data['id_book'][0] = $infomation_borrowBook[0]['id_book'];
                $data['id_account'] = $infomation_borrowBook[0]['id_account'];
                $data['time_end'] = $infomation_borrowBook[0]['time_end'];

                $data['status_book'] = "Available";
                $data['status_log_book'] = "Returned";
                $data['status_book_started_before'] = "Borrowed";
                $bookStartedBeforeModel = $this->get_model("BookStartedBeforeModel");

                if(strtotime($data['time_end']) < strtotime(date('Y-m-d H:i:s')))
                {
                    $data['status_log_book'] = "Expired";
                }

                $Book = $this->get_model("BookModel");
                if(!empty($message))
                {
                    echo json_encode($this->form_json('400', "", $message));
                    return json_encode($this->form_json('400', "", $message));
                }
            }
            else
            {
                    echo json_encode($this->form_json('404', "", "Không tìm thấy mã mượn sách !"));
                    return json_encode($this->form_json('404', "", "Không tìm thấy mã mượn sách !"));
            }

            $result = $borrowBook->give_book_back($data);
            $borrowBook->close_connect();
            if($result)
            {
                //cập nhập lại status book
                $Book->update_status($data);
                $Book->close_connect();
                //cập nhập lại trạng thái đặt trước
                $bookStartedBeforeModel->update_status($data);
                echo json_encode($this->form_json('200', $result, 'Đã hoàn thành quá trình trả sách !'));
                return json_encode($this->form_json('200', $result, 'Đã hoàn thành quá trình trả sách !'));
            }
            else
            {
                echo json_encode($this->form_json('400', '', 'Không trả được sách, vui lòng thực hiện lại !'));
                return json_encode($this->form_json('400', '', 'Không trả được sách, vui lòng thực hiện lại !'));
            }
        }
        else
        {
            echo json_encode($this->form_json('403', '', 'Request method is incorrect !'));
            return json_encode($this->form_json('403', '', 'Request method is incorrect !'));
        }
    }
}