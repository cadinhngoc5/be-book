<?php
class BookModel extends BaseModel
{
    private $db_table = "info_book";
    public function __construct()
    {
        $this->conn = $this->connectDb();
    }

    //create book dẫ sửa
    public function create_book($data)
    {
        $query = "INSERT INTO " . $this->db_table . "(book_name, book_type, book_describe, book_author, book_publisher, book_cost, book_image, book_language) 
                    VALUES('" . $data['book_name'] . "','" . $data['book_type'] . "','" . $data['book_describe'] . "',
                    '" . $data['book_author'] . "','" . $data['book_publisher'] . "',
                    '" . $data['book_cost'] . "','" . $data['book_image'] . "','" . $data['book_language'] . "')";
        $result = $this->conn->query($query);
        if ($result)
        {
            return true;
        }
        return false;
    }

    // get all book đã vứt phân trang
    public function get_all_book()//$pagination, $search
    {
        //SELECT info_book.id, info_book.ten, info_book.loai_sach, info_book.mo_ta, tac_gia.ten , nha_xuat_ban.ten, info_book.so_luong, info_book.gia_ban, info_book.anh, info_book.ngon_ngu, info_book.gia_thue_tren_ngay, info_book.ngay_tao FROM info_book JOIN tac_gia ON info_book.id_tac_gia = tac_gia.id JOIN nha_xuat_ban ON info_book.id_nha_xuat_ban = nha_xuat_ban.id WHERE tac_gia.ten LIKE '%oda%' LIMIT 10 OFFSET 0;
//        $offset = ($pagination['page_number'] - 1)* $pagination['page_size'];
//        $search_query = " WHERE ".$search['search_field']." LIKE '%".$search['search_content']."%' ";
//         .$search_query. " LIMIT ". $pagination['page_size']. " OFFSET ".$offset
        $query = "SELECT *
        FROM ". $this->db_table." where status !='Delete'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
        }
        return $data;
    }



    //GET SINGLE BOOK đã sửa
    public function get_single_book($id_book)
    {
        $query = "SELECT * FROM ". $this->db_table ." WHERE id ='$id_book'";
        $result = $this->conn->query($query);
        if ($result->num_rows > 0)
        {
            return $result->fetch_assoc();
        }
        return false;
    }

    // UPDATE da sửa
    public function update_book($data)
    {
        $queryCheck = "SELECT id FROM ".$this->db_table." WHERE id = '".$data['id']."'";
        $result = $this->conn->query($queryCheck);
        if($result->num_rows > 0)
        {
            $query = "UPDATE ". $this->db_table ." SET book_name='".$data['book_name']."',book_type='".$data['book_type']."', book_describe='".$data['book_describe']."',
            book_author='".$data['book_author']."',book_publisher='".$data['book_publisher']."',
            book_cost='".$data['book_cost']."', book_image='".$data['book_image']."' ,book_language='".$data['book_language']."'
            WHERE id = '".$data['id']."'";
            $this->conn->query($query);
            return true;
        }
        return false;
    }

    //update status của sách
    public function update_status($data)
    {
        for($i = 0; $i < Count($data['id_book']); $i++)
        {
            $query = "UPDATE ". $this->db_table ." SET status='".$data['status_book']."' WHERE id = '".$data['id_book'][$i]."'";
            $this->conn->query($query);
        }
        return true;
    }

    // DELETE book chắc là phải bỏ
    public function delete($id_book)
    {
        $queryCheck = "SELECT id FROM ".$this->db_table." WHERE id = '$id_book'";
        $result = $this->conn->query($queryCheck);
        if($result->num_rows > 0)
        {
            $sqlQuery = "DELETE FROM ".$this->db_table." WHERE id = '$id_book'";
            $this->conn->query($sqlQuery);
            return true;
        }
        return false;
    }

    //update số lượng sách//liên quan đến mượn sách       PHẢI BỎ NHƯNG ĐỂ CŨNG KHÔNG SAO :)
//    public function update_so_luong($data)
//    {
//        for($i = 0; $i < Count($data['id_book']); $i++)
//        {
//            $query = "UPDATE ". $this->db_table ." SET so_luong='".$data['so_luong_sach'][$i] - 1 ."' WHERE id = '".$data['id_book'][$i]."'";
//            $this->conn->query($query);
//        }
//        return true;
//    }

//    public function get_so_luong($search)
//    {
//        $search_query = " WHERE ".$search['search_field']." LIKE '%".$search['search_content']."%' ";
//        $query = "SELECT COUNT(id) AS so_luong FROM ".$this->db_table.$search_query;
//        $so_luong = $this->conn->query($query)->fetch_assoc();
//        return $so_luong['so_luong'];
//    }

}
?>

