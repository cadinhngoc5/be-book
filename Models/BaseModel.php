<?php
class BaseModel
{
    public function connectDb()
    {
        $hostname = "localhost";
        $username = "root";
        $password = "";
        $db = "data_book";

        $conn = new mysqli($hostname, $username, $password, $db);
        if($conn->connect_errno)
        {
            die("Lỗi khi kết nối MySQL");
        }
        return $conn;
    }
    // close connect database
    public function close_connect()
    {
        mysqli_close($this->conn);
    }
}
?>
