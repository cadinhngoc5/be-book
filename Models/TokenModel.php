<?php
class TokenModel extends BaseModel
{
    private $db_table = "token_users";

    public function __construct()
    {
        $this->conn = $this->connectDb();
    }

    //create token
    public function create_token($id_user)
    {
        $token = bin2hex(random_bytes(40));
        $refresh_token = bin2hex(random_bytes(40));

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $token_expried = date('Y-m-d H:i:s', strtotime('+30 day'));
        $refresh_token_expried = date('Y-m-d H:i:s', strtotime('+365 day'));

        $query = "INSERT INTO ".$this->db_table."(token, refresh_token, token_expried, refresh_token_expried, id_user) 
                    VALUES('" . $token . "','" . $refresh_token . "','" . $token_expried . "','" . $refresh_token_expried . "','" . $id_user . "')";
        $result = $this->conn->query($query);
        if ($result)
        {
            $query1 = "SELECT * FROM ".$this->db_table." WHERE id_user='$id_user'";
            return $this->conn->query($query1)->fetch_assoc();
        }
        return false;
    }

    //check token
    public function check_token($id_user)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE id_user='$id_user'";
        $result = $this->conn->query($query);
        if ($result->num_rows > 0)
        {
            return $result->fetch_assoc();
        }
        return false;
    }

    //update token
    public function update_token($token_old)
    {
        $queryCheck = "SELECT * FROM ".$this->db_table." WHERE token='$token_old'";
        $resultCheck = $this->conn->query($queryCheck);
        if($resultCheck->num_rows > 0)
        {
            $token = bin2hex(random_bytes(40));
            $refresh_token = bin2hex(random_bytes(40));
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $token_expried = date('Y-m-d H:i:s', strtotime('+30 day'));
            //$refresh_token_expried = date('Y-m-d H:i:s', strtotime('+365 day'));
            $refresh_token_expried = $resultCheck->fetch_assoc()['refresh_token_expried'];

            $query = "UPDATE ".$this->db_table." SET token='".$token."',refresh_token='".$refresh_token."', token_expried='".$token_expried."',
                    refresh_token_expried='".$refresh_token_expried."' WHERE token = '$token_old'";
            $this->conn->query($query);

            $query_info_token = "SELECT * FROM ".$this->db_table." WHERE token='$token'";
            return $this->conn->query($query_info_token)->fetch_assoc();
        }
        return false;
    }

    //check token from header
    public function check_token_from_header($token_from_header)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE token='$token_from_header'";
        $result = $this->conn->query($query);

        if ($result->num_rows > 0)
        {
            return $result->fetch_assoc();
        }
        return false;
    }

    //DELETE Token with token
    public function delete_token_with_token($token)
    {
        $queryCheck = "SELECT id FROM ".$this->db_table." WHERE token = '$token'";
        $result = $this->conn->query($queryCheck);
        if($result->num_rows > 0)
        {
            $sqlQuery = "DELETE FROM ".$this->db_table." WHERE token = '$token'";
            $this->conn->query($sqlQuery);
            return true;
        }
        return false;
    }
}
