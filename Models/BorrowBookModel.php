<?php
class BorrowBookModel extends BaseModel
{
    private $db_table = "log_book";
    public function __construct()
    {
        $this->conn = $this->connectDb();
    }

    // tra sach
    //update status của logbook
    public function give_book_back($data)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $return_date = date('Y-m-d H:i:s');
        for($i = 0; $i < Count($data['id_book']); $i++)
        {
            $query = "UPDATE ". $this->db_table ." SET status='".$data['status_log_book']."', return_date='".$return_date."' WHERE id = '".$data['id_log_book']."'";
            $this->conn->query($query);
        }
        return true;
    }
    //get by id phiếu mượn
    public function get_borrow_book_by_id_log_book($id_log_book)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE id = '$id_log_book'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    //update status của logbook
    public function update_status($data)
    {
        for($i = 0; $i < Count($data['id_book']); $i++)
        {
            $query = "UPDATE ". $this->db_table ." SET status='".$data['status_log_book']."' WHERE id = '".$data['id_book'][$i]."'";
            $this->conn->query($query);
        }
        return true;
    }

    //create logBook
    public function borrow_book($data)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $time_start = date('Y-m-d H:i:s');
        $time_end = "+".$data['borrow_days']." day";
        $time_end = date('Y-m-d H:i:s', strtotime($time_start. $time_end));

        for($i = 0; $i < Count($data['id_book']); $i++)
        {
            $query = "INSERT INTO ".$this->db_table."(id_book, id_account, time_start, time_end)
                    VALUES('" . $data['id_book'][$i] . "','" . $data['id_account'] . "','" . $time_start . "','" . $time_end . "')";
            $result = $this->conn->query($query);
        }
        if ($result)
        {
            $query_last_id = "SELECT * FROM ".$this->db_table." WHERE id=(SELECT MAX(id) FROM ".$this->db_table.");";
            $result_last_item = $this->conn->query($query_last_id);
            if ($result)
            {
                return $result_last_item->fetch_assoc();
            }
        }
        return false;
    }

    public function get_borrowing_book_by_id_user($data)//lấy hết id sách 20-12-2022
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE id_account = '".$data['id_user']."' and status = '".$data['status_log_book']."'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_history_book_by_id_user($data)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE id_account = '".$data['id_user']."' and status = '".$data['status_log_book']."'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_borrowed_book_by_id_user($data)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE id_account = '".$data['id_user']."' and status != '".$data['status_log_book']."'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    //get all borrowed
    public function get_all_borrowed_book($data)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE status != '".$data['status_log_book']."'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    //get all borrowing
    public function get_all_borrowing_book($data)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE status = '".$data['status_log_book']."'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    //get logBook
    public function get_all_borrow_book()
    {
        $query = "SELECT * FROM ".$this->db_table;
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    //update logBook
    public function update()
    {
        return true;
    }

    public function count_book_borrowed($id_account)
    {
        $query = "SELECT COUNT(id_account) AS so_luong FROM ".$this->db_table. " 
        WHERE id_account = '".$id_account."' and status != 'Returned'";
        $so_luong = $this->conn->query($query)->fetch_assoc();
        return $so_luong['so_luong'];
    }

    public function get_expired_all_borrowed_book()
    {
        $query = "SELECT * FROM ".$this->db_table." where return_date IS NULL and time_end < CURRENT_TIMESTAMP()";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_expired_borrowed_book_by_id_user($id_user)
    {
        $query = "SELECT * FROM ".$this->db_table." where return_date IS NULL and time_end < CURRENT_TIMESTAMP() and id_account = '".$id_user."'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    //delete by id book
    public function delete_by_id_book($id_book)
    {
        $queryCheck = "SELECT id FROM ".$this->db_table." WHERE id_book = '$id_book'";
        $result = $this->conn->query($queryCheck);
        if($result->num_rows > 0)
        {
            $sqlQuery = "DELETE FROM ".$this->db_table." WHERE id_book = '$id_book'";
            $this->conn->query($sqlQuery);
            return true;
        }
        return false;
    }

    // DELETE logBook
    public function delete($id_user)
    {
        $queryCheck = "SELECT id FROM ".$this->db_table." WHERE id_account = '$id_user'";
        $result = $this->conn->query($queryCheck);
        if($result->num_rows > 0)
        {
            $sqlQuery = "DELETE FROM ".$this->db_table." WHERE id_account = '$id_user'";
            $this->conn->query($sqlQuery);
            return true;
        }
        return false;
    }
}

