<?php
//include("BaseModel.php");

class  UserModel extends BaseModel
{
    private $db_table = "account";

    public function __construct()
    {
        $this->conn = $this->connectDb();
    }

    //update status của user
    public function update_status($data)
    {
        $query = "UPDATE ". $this->db_table ." SET status='".$data['status_user']."' WHERE id = '".$data['id_user']."'";
        $this->conn->query($query);
        return true;
    }
    // GET ALL
    public function get_all_user()
    {
        $query = "SELECT * FROM " . $this->db_table. " WHERE user_type = '2'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }
        return $data;
    }

    // CREATE
    public function create($data)
    {
        $query = "INSERT INTO " . $this->db_table . "(email, phone_number, user_name, user_password) 
                    VALUES('".$data['email']."','".$data['phone_number']."','".$data['user_name']."','".$data['user_password']."')";
        $result = $this->conn->query($query);
        if ($result)
        {
            return true;
        }
        return false;
    }

    // READ single
    public function get_single_user($id_user)
    {
        $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE id ='$id_user'";
        $result = $this->conn->query($sqlQuery);
        if($result->num_rows > 0)
        {
            return $result->fetch_assoc();
        }
        return false;
    }

    //read by name
    public function get_user_by_username($userName)
    {
        $sqlQ = "SELECT * FROM " . $this->db_table. " WHERE user_name = '$userName'";
        $result = $this->conn->query($sqlQ)->fetch_assoc();
        return $result;
    }

    // UPDATE
    public function update_user($data)
    {
        $queryCheck = "SELECT id FROM ".$this->db_table." WHERE id = '".$data['id']."'";
        $result = $this->conn->query($queryCheck);
        $time_update = date('Y-m-d H:i:s');
        if($result->num_rows > 0)
        {
            $query = "UPDATE " . $this->db_table . " SET lastname='".$data['lastname']."',firstname='".$data['firstname']."', email='".$data['email']."',
            phone_number='".$data['phone_number']."',address='".$data['address']."', update_date = '".$time_update."', sex='".$data['sex']."', date_of_birth='".$data['date_of_birth']."' WHERE id ='".$data['id']."'";
            $this->conn->query($query);
            return true;
        }
        return false;
    }

    // DELETE
    public function delete($id_user)
    {
        $queryCheck = "SELECT id FROM ".$this->db_table." WHERE id = '".$id_user."'";
        $result = $this->conn->query($queryCheck);
        if($result->num_rows > 0)
        {
            $sqlQuery = "DELETE FROM ".$this->db_table." WHERE id = '".$id_user."'";
            $this->conn->query($sqlQuery);
            return true;
        }
        return false;
    }

    //login user
    public function check_login($data)
    {
        $query = "SELECT user_name, user_password FROM " . $this->db_table . " WHERE user_name = '" . $data['user_name'] . "' and user_password = '" . $data['user_password'] . "'";
        $result = $this->conn->query($query);
        if ($result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    //check user_password
    public function check_password($data)
    {
        $query = "SELECT user_name FROM " . $this->db_table . " WHERE id = '" . $data['id_user'] . "' and user_password = '" . $data['old_password'] . "'";
        $result = $this->conn->query($query);
        if ($result->num_rows > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // update password
    public function change_password($data)
    {
        $time_update = date('Y-m-d H:i:s');
        $query = "UPDATE " . $this->db_table . " SET  user_password='".$data['new_password']."',
            update_date = '".$time_update."' WHERE id ='".$data['id_user']."'";
        $result = $this->conn->query($query);
        if($result)
        {
            return true;
        }
        return false;
    }

    public function check_field_existed($field, $value)//kiểm tra sự tồn tại của các trường
    {
        $query = "SELECT id FROM " . $this->db_table . " 
        WHERE ".$field." = '".$value."'";
        $result = $this->conn->query($query);
        if ($result->num_rows > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //DELETE Token with id_user vì bị lỗi linh tinh nên phải viết vào đây
    public function delete_token_with_id_user($id_user)
    {
        $queryCheck = "SELECT id FROM token_users WHERE id_user = '$id_user'";
        $result = $this->conn->query($queryCheck);
        if($result->num_rows > 0)
        {
            $sqlQuery = "DELETE FROM token_users WHERE id_user = '$id_user'";
            $this->conn->query($sqlQuery);
            return true;
        }
        return false;
    }

    public function get_user_type($id_user)
    {
        $queryCheck = "SELECT user_type FROM ".$this->db_table." WHERE id = '".$id_user."'";
        $result = $this->conn->query($queryCheck);
        if ($result->num_rows > 0)
        {
            return $result->fetch_assoc();
        }
        else
        {
            return false;
        }
    }
}
?>