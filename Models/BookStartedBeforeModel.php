<?php
//xong
class BookStartedBeforeModel extends BaseModel
{
    private $db_table = "book_started_before";
    public function __construct()
    {
        $this->conn = $this->connectDb();
    }

    //phương thức đặt trước sách
    public function book_started_before($data)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $intend_date = $data['intend_date'];
        $expiration_date = date('Y-m-d H:i:s', strtotime($intend_date. ' + 2 days'));

        for($i = 0; $i < Count($data['id_book']); $i++)
        {
            $query = "INSERT INTO ".$this->db_table."(id_book, id_account, intend_date, expiration_date)
                    VALUES('" . $data['id_book'][$i] . "','" . $data['id_account'] . "','" . $intend_date . "','" . $expiration_date . "')";
            $result = $this->conn->query($query);
        }
        $query_last_id = "SELECT * FROM ".$this->db_table." WHERE id=(SELECT MAX(id) FROM ".$this->db_table.");";
        $result_last_item = $this->conn->query($query_last_id);

        if ($result)
        {
            return $result_last_item->fetch_assoc();
        }
        return false;
    }

    //lấy danh sách sách đặt trước
    public function get_all_list()
    {
        $query = "SELECT * FROM ".$this->db_table;
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_book_started_before_by_id_user($id_user)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE id_account = '$id_user' and status = 'Waiting'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
//get by id phiếu đặt trước
    public function get_book_started_before_by_id_promissory_note($id_promissory_note)
    {
        $query = "SELECT * FROM ".$this->db_table." WHERE id = '$id_promissory_note'";
        $result = $this->conn->query($query);
        $data = [];
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    //sửa trạng thái của đơn đặt trước
    //update status của sách
    public function update_status($data)
    {
        for($i = 0; $i < Count($data['id_book']); $i++)
        {
            $query = "UPDATE ". $this->db_table ." SET status='".$data['status_book_started_before']."' WHERE id_book = '".$data['id_book'][$i]."'";
            $this->conn->query($query);
        }
        return true;
    }

    public function count_book_started_before($id_account)
    {
        $query = "SELECT COUNT(id_account) AS so_luong FROM ".$this->db_table. " 
        WHERE id_account = '".$id_account."' and status != 'Borrowed'";
        $so_luong = $this->conn->query($query)->fetch_assoc();
        return $so_luong['so_luong'];
    }

}


